package com.josh_hansen.artificialhorizon;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class ArtificialHorizonView extends SurfaceView implements SurfaceHolder.Callback {
    private ArtificialHorizonThread horizonThread;
    private String TAG = "ArtificialHorizonView";
    private Context mContext;
    private Bitmap mBackgroundBmp;
    private float backgroundRotation = 0;
    private float roll;
    private float pitch;
    private Matrix transform;
    private TiltHandler tiltHandler;

    public ArtificialHorizonView(Context context, AttributeSet attrs) {
        super(context, attrs);
        getHolder().addCallback(this);
        mContext = context;
        tiltHandler = new TiltHandler(context);
        tiltHandler.beginTilt();
        Log.e(TAG,"Constructor");
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Log.e(TAG,"surfaceCreated");
        setWillNotDraw(false);
        transform = new Matrix();
        Bitmap backgroundBmpOrg = BitmapFactory.decodeResource(getResources(), R.drawable.horizon_background);
        mBackgroundBmp = Bitmap.createScaledBitmap(backgroundBmpOrg,getWidth()*2,getHeight()*3,true);
        horizonThread = new ArtificialHorizonThread(getHolder(),this);
        horizonThread.setRunning(true);
        horizonThread.start();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        try {
            horizonThread.setRunning(false);
            horizonThread.join();
        } catch (InterruptedException e) {
            Log.e(TAG, "" + e);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        //canvas.drawColor(Color.BLACK);

        rotateAndDraw(canvas, mBackgroundBmp.getWidth(), mBackgroundBmp.getHeight(), mBackgroundBmp, (float)tiltHandler.getPitchDegrees());
        if(backgroundRotation > 359) {
            backgroundRotation = 0;
        }
    }

    public void rotateAndDraw(Canvas canvas, int bmpWidth, int bmpHeight, Bitmap bmp, float rotationAngle){
        //canvas.save();
        transform.reset();
        transform.postRotate(rotationAngle, bmpWidth / 2, bmpHeight / 2);
        transform.postTranslate(-(canvas.getWidth()/2),-(canvas.getHeight()));
        canvas.drawBitmap(bmp, transform, null);

        //canvas.restore();
    }

    public void setRoll(float rollIn) {
        Log.e(TAG," "+rollIn);
        roll= rollIn;
    }

    public ArtificialHorizonThread getThread() {
        return horizonThread;
    }

    class ArtificialHorizonThread extends  Thread {
        private SurfaceHolder mSurfaceHolder;
        private ArtificialHorizonView mArtificialHorizonView;
        private boolean mRun = false;


        public ArtificialHorizonThread(SurfaceHolder surfaceHolder, ArtificialHorizonView artificialHorizonView) {
            mArtificialHorizonView =  artificialHorizonView;
            mSurfaceHolder = surfaceHolder;
        }

        public void setRunning(boolean run) {
            mRun = run;
        }

        @Override
        public void run() {
            Canvas canvas;
            while(mRun) {
                canvas = null;
                try {
                    canvas = mSurfaceHolder.lockCanvas();
                    synchronized (mSurfaceHolder) {

                        postInvalidate();
                    }
                } finally {
                    if(canvas != null) {
                        mSurfaceHolder.unlockCanvasAndPost(canvas);
                    }
                }
            }
        }
    }
}

